﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Escalas
    {

        private int id_escala;
        private string pais, aeropuerto, fecha, hora;
        
        Conexion con;

        public Escalas()
        {
        }

        public Escalas(int id_escala, string pais, string aeropuerto, string fecha, string hora)
        {
            this.pais = pais;
            this.aeropuerto = aeropuerto;
            this.fecha = fecha;
            this.hora = hora;
        }

        public Escalas(string pais, string aeropuerto, string fecha, string hora)
        {
            this.pais = pais;
            this.aeropuerto = aeropuerto;
            this.fecha = fecha;
            this.hora = hora;
        }

        public void registrarEscalas()
        {
            con = new Conexion();

            string SQL = "INSERT INTO escalas (pais, aeropuerto, fecha, hora) VALUES (@pais, @aeropuerto, @fecha, @hora);";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("pais", pais);
            cmd.Parameters.AddWithValue("aeropuerto", aeropuerto);
            cmd.Parameters.AddWithValue("fecha", fecha);
            cmd.Parameters.AddWithValue("hora", hora);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarEscalas()
        {
            con = new Conexion();
            string SQL = "DELETE FROM escalas WHERE id_escala = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_escala);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Escalas> obtenerEscalas()
        {
            List<Escalas> listEscalas = new List<Escalas>();
            con = new Conexion();
            string SQL = "SELECT * FROM escalas";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.NextResult())
            {
                listEscalas.Add(new Escalas(record.GetInt32(0), record.GetString(1),
                    record.GetString(2), record.GetString(3), record.GetString(4)));
            }
            return listEscalas;
        }

        public List<string> obtenerEscalasSinRepetir()
        {
            List<string> listEscalas = new List<string>();
            con = new Conexion();
            string SQL = "SELECT pais FROM escalas";
            string iterador;
            Boolean añadir = true;

            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());
            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.NextResult())
            {
                string actual = record.GetString(0).Trim();
                for (int i = 0; i < listEscalas.Count(); i++)
                {
                    if (añadir)
                    {
                        iterador = listEscalas[i];
                        if (iterador.Equals(actual))
                        {
                            añadir = false;
                        }
                    }
                }
                if (añadir)
                {
                    listEscalas.Add(actual);
                }
                añadir = true;
            }
                return listEscalas;
        }

        public List<Escalas> obtenerEscalasPorPais(string pais)
        {
            List<Escalas> listEscalas = new List<Escalas>();
            con = new Conexion();
            string SQL = "SELECT * FROM escalas Where pais = @pais";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("pais", pais);
            cmd.Prepare();

            NpgsqlDataReader record = cmd.ExecuteReader();

            while (record.NextResult())
            {
                listEscalas.Add(new Escalas(record.GetInt32(0), record.GetString(1), record.GetString(2),
                    record.GetString(3), record.GetString(4)));
            }
            return listEscalas;
        }
    }
}

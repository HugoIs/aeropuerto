﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Avion
    {
        public int id_avion, id_aerolinea, capacidad;
        public string modelo, disponibilidad;

        Conexion con;

        public Avion()
        {
        }

        public Avion(int id_aerolinea)
        {
            this.id_aerolinea = id_aerolinea;
        }

        public Avion(int id_aerolinea, string modelo, int capacidad, string disponibilidad)
        {
            this.id_aerolinea = id_aerolinea;
            this.modelo = modelo;
            this.capacidad = capacidad;
            this.disponibilidad = disponibilidad;
        }

        public Avion(int id_avion, int id_aerolinea, string modelo, int capacidad, string disponibilidad)
        {
            this.id_avion = id_avion;
            this.id_aerolinea = id_aerolinea;
            this.modelo = modelo;
            this.capacidad = capacidad;
            this.disponibilidad = disponibilidad;
        }

        public void registrarAvion()
        {
            con = new Conexion();

            string SQL = "Insert Into avion(id_aerolinea, modelo, capacidad, disponibilidad) values(@id, @modelo, @capacidad, @disponibilidad)";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_aerolinea);
            cmd.Parameters.AddWithValue("modelo", modelo);
            cmd.Parameters.AddWithValue("capacidad", capacidad);
            cmd.Parameters.AddWithValue("disponibilidad", disponibilidad);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void modificarAvion()
        {
            con = new Conexion();
            //hacer mi consulta 
            string mo = "UPDATE avion SET id_aerolinea=@ida, modelo=@modelo, capacidad=@capacidad," +
                "disponibilidad=@disponibilidad WHERE id_avion=@id";
            var cmd = new NpgsqlCommand(mo, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("ida", id_aerolinea);
            cmd.Parameters.AddWithValue("modelo", modelo);
            cmd.Parameters.AddWithValue("capacidad", capacidad);
            cmd.Parameters.AddWithValue("disponibilidad", disponibilidad);
            cmd.Parameters.AddWithValue("id", id_avion);

            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarAvion()
        {
            con = new Conexion();

            string SQL = "Delete From avion where id_avion = ?";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());
            
            cmd.Parameters.AddWithValue("id", id_avion);

            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public void eliminarPorAerolinea()
        {
            con = new Conexion();

            string SQL = "Delete From avion where id_aerolinea = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_aerolinea);

            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public List<Avion> obtenerAviones()
        {
            List<Avion> listAviones = new List<Avion>();
            con = new Conexion();
            string SQL = "Select * From avion";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listAviones.Add(new Avion(record.GetInt32(0), record.GetInt32(1), record.GetString(2),
                    record.GetInt32(2), record.GetString(3)));
            }
            return listAviones;
        }
    }
}

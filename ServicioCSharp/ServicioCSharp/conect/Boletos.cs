﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Boletos
    {
        public int id_boleto, num_vuelo, id_usuario;
        public string clase, codigo;

        Conexion con;

        public Boletos()
        {
        }

        public Boletos(int num_vuelo)
        {
            this.num_vuelo = num_vuelo;
        }

        public Boletos(int id_boleto, int num_vuelo, int id_usuario, string clase, string codigo)
        {
            this.id_boleto = id_boleto;
            this.num_vuelo = num_vuelo;
            this.id_usuario = id_usuario;
            this.clase = clase;
            this.codigo = codigo;
        }

        public Boletos(int num_vuelo, int id_usuario, string clase, string codigo)
        {
            this.num_vuelo = num_vuelo;
            this.id_usuario = id_usuario;
            this.clase = clase;
            this.codigo = codigo;
        }

        public void registrarBoleto()
        {
            con = new Conexion();

            string SQL = "Insert Into Boletos(num_vuelo, id_usuario, clase, codigo) VALUES(@num_vuelo, @id, @clase, @codigo)";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("num_vuelo", num_vuelo);
            cmd.Parameters.AddWithValue("id", id_usuario);
            cmd.Parameters.AddWithValue("clase", clase);
            cmd.Parameters.AddWithValue("codigo", codigo);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarBoleto()
        {
            con = new Conexion();

            string SQL = "Delete From boletos where id_boleto = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_boleto);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarPorVuelo()
        {
            con = new Conexion();

            string SQL = "Delete From boletos where num_vuelo = @num_vuelo";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("num_vuelo", num_vuelo);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Boletos> obtenerBoletos()
        {
            List<Boletos> listBoletos = new List<Boletos>();
            con = new Conexion();
            string SQL = "Select * From boletos where id_boleto = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_boleto);
            cmd.Prepare();

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listBoletos.Add(new Boletos(record.GetInt32(0), record.GetInt32(1), record.GetInt32(2),
                    record.GetString(3), record.GetString(4)));
            }
            return listBoletos;
        }

        public int maxBoletos(int id, int resta)
        {
            int contador = 0;
            con = new Conexion();
            string SQL = "Select id_boleto From boletos Where num_vuelo = @num_vuelo";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("num_vuelo", num_vuelo);
            cmd.Prepare();

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                contador++;
            }
            contador -= resta;
            return contador;
        }
    }
}

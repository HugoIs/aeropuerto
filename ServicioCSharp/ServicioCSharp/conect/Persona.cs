﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCsharp.Persona
{
    public class Persona
    {
        public string nombre, curp, apellidos, correo, contra;
        Conexion con;
        private int id_persona { get; set; }

        public Persona()
        {
        }

        public Persona(string nombre, string apellidos)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
        }

        public Persona(int id_persona, string nombre, string apellidos, string curp, string correo, string contra)
        {
            this.id_persona = id_persona;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.curp = curp;
            this.correo = correo;
            this.contra = contra;
        }

        public Persona(string nombre, string apellidos, string curp, string correo, string contra)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.curp = curp;
            this.correo = correo;
            this.contra = contra;
        }

        public void registrarPersona()
        {
            con = new Conexion();

            //String con la conexion de la base
            string SQL = "INSERT INTO persona(nombre, apellidos, curp, nivel)" +
                    "VALUES( @nombre, @apellidos, @curp, 1)";

            //Crear un comando de para postgresql
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("apellidos", apellidos);
            cmd.Parameters.AddWithValue("curp", curp);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void modificarPersona()
        {
            con = new Conexion();
            //hacer mi consulta 
            string mo = "UPDATE persona SET nombre=@nombre,apellidos=@apellidos, curp=@curp, correo=@correo, contra=@contra WHERE id_persona=@id";
            var cmd = new NpgsqlCommand(mo, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("apellidos", apellidos);
            cmd.Parameters.AddWithValue("curp", curp);
            cmd.Parameters.AddWithValue("correo", correo);
            cmd.Parameters.AddWithValue("contra", contra);
            cmd.Parameters.AddWithValue("id", id_persona);

            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarPersona()
        {
            con = new Conexion();

            string consulta = "DELETE FROM persona Where curp=@curp";
            var cmd = new NpgsqlCommand(consulta, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("curp", curp);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Persona> obtenerPersonas()
        {
            List<Persona> listPersonas = new List<Persona>();

            con = new Conexion();
            String SQL = "Select * from persona";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.Read())
            {
                listPersonas.Add(new Persona(record.GetInt32(0), record.GetString(1), record.GetString(2), record.GetString(3),
                        record.GetString(4), record.GetString(5)));
            }

            return listPersonas;
        }
    }
}
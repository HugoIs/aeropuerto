﻿using Npgsql;
using ProyectoCsharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoServicioCSharp.connect
{
    public class Vuelos
    {

        private int num_vuelo, id_avion, id_ruta;
        Conexion con;

        public Vuelos()
        {
        }

        public Vuelos(int id_avion, int id_ruta)
        {
            this.id_avion = id_avion;
            this.id_ruta = id_ruta;
        }

        public Vuelos(int num_vuelo, int id_avion, int id_ruta)
        {
            this.num_vuelo = num_vuelo;
            this.id_avion = id_avion;
            this.id_ruta = id_ruta;
        }

        public void registrarVuelo()
        {
            con = new Conexion();

            string SQL = "Insert Into vuelos(id_avion, id_ruta) values(@ida, @idr)";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("ida", id_avion);
            cmd.Parameters.AddWithValue("idr", id_ruta);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarVuelo()
        {
            con = new Conexion();

            string SQL = "Delete From vuelos where num_vuelo = @num_vuelo";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("num_vuelo", num_vuelo);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public void eliminarPorAvion()
        {
            con = new Conexion();

            string SQL = "Delete From vuelos where id_avion = @id";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            cmd.Parameters.AddWithValue("id", id_avion);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
        }

        public List<Vuelos> obtenerVuelos()
        {
            List<Vuelos> listVuelos = new List<Vuelos>();
            con = new Conexion();
            string SQL = "Select * From vuelos";
            var cmd = new NpgsqlCommand(SQL, con.ObtenerConexion());

            NpgsqlDataReader record = cmd.ExecuteReader();
            while (record.NextResult())
            {
                listVuelos.Add(new Vuelos(record.GetInt32(0), record.GetInt32(1), record.GetInt32(2)));
            }
            return listVuelos;
        }
    }
}


package org.aeropuerto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_avion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_ruta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idAvion",
    "idRuta"
})
@XmlRootElement(name = "InsertarVuelos")
public class InsertarVuelos {

    @XmlElement(name = "id_avion")
    protected int idAvion;
    @XmlElement(name = "id_ruta")
    protected int idRuta;

    /**
     * Obtiene el valor de la propiedad idAvion.
     * 
     */
    public int getIdAvion() {
        return idAvion;
    }

    /**
     * Define el valor de la propiedad idAvion.
     * 
     */
    public void setIdAvion(int value) {
        this.idAvion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRuta.
     * 
     */
    public int getIdRuta() {
        return idRuta;
    }

    /**
     * Define el valor de la propiedad idRuta.
     * 
     */
    public void setIdRuta(int value) {
        this.idRuta = value;
    }

}

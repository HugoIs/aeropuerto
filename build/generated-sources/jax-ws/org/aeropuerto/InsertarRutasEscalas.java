
package org.aeropuerto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_ruta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_escala" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="contexto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idRuta",
    "idEscala",
    "contexto"
})
@XmlRootElement(name = "InsertarRutas_Escalas")
public class InsertarRutasEscalas {

    @XmlElement(name = "id_ruta")
    protected int idRuta;
    @XmlElement(name = "id_escala")
    protected int idEscala;
    protected int contexto;

    /**
     * Obtiene el valor de la propiedad idRuta.
     * 
     */
    public int getIdRuta() {
        return idRuta;
    }

    /**
     * Define el valor de la propiedad idRuta.
     * 
     */
    public void setIdRuta(int value) {
        this.idRuta = value;
    }

    /**
     * Obtiene el valor de la propiedad idEscala.
     * 
     */
    public int getIdEscala() {
        return idEscala;
    }

    /**
     * Define el valor de la propiedad idEscala.
     * 
     */
    public void setIdEscala(int value) {
        this.idEscala = value;
    }

    /**
     * Obtiene el valor de la propiedad contexto.
     * 
     */
    public int getContexto() {
        return contexto;
    }

    /**
     * Define el valor de la propiedad contexto.
     * 
     */
    public void setContexto(int value) {
        this.contexto = value;
    }

}

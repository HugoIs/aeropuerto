
package org.aeropuerto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="costoPorVuelo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "costoPorVuelo"
})
@XmlRootElement(name = "InsertarRutas")
public class InsertarRutas {

    protected double costoPorVuelo;

    /**
     * Obtiene el valor de la propiedad costoPorVuelo.
     * 
     */
    public double getCostoPorVuelo() {
        return costoPorVuelo;
    }

    /**
     * Define el valor de la propiedad costoPorVuelo.
     * 
     */
    public void setCostoPorVuelo(double value) {
        this.costoPorVuelo = value;
    }

}

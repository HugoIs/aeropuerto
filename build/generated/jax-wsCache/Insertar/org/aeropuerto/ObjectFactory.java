
package org.aeropuerto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.aeropuerto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.aeropuerto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertarAerolineaResponse }
     * 
     */
    public InsertarAerolineaResponse createInsertarAerolineaResponse() {
        return new InsertarAerolineaResponse();
    }

    /**
     * Create an instance of {@link InsertarAvionResponse }
     * 
     */
    public InsertarAvionResponse createInsertarAvionResponse() {
        return new InsertarAvionResponse();
    }

    /**
     * Create an instance of {@link InsertarBoletos }
     * 
     */
    public InsertarBoletos createInsertarBoletos() {
        return new InsertarBoletos();
    }

    /**
     * Create an instance of {@link InsertarPersonaResponse }
     * 
     */
    public InsertarPersonaResponse createInsertarPersonaResponse() {
        return new InsertarPersonaResponse();
    }

    /**
     * Create an instance of {@link InsertarEscalas }
     * 
     */
    public InsertarEscalas createInsertarEscalas() {
        return new InsertarEscalas();
    }

    /**
     * Create an instance of {@link InsertarPersona }
     * 
     */
    public InsertarPersona createInsertarPersona() {
        return new InsertarPersona();
    }

    /**
     * Create an instance of {@link InsertarRutasResponse }
     * 
     */
    public InsertarRutasResponse createInsertarRutasResponse() {
        return new InsertarRutasResponse();
    }

    /**
     * Create an instance of {@link InsertarAerolinea }
     * 
     */
    public InsertarAerolinea createInsertarAerolinea() {
        return new InsertarAerolinea();
    }

    /**
     * Create an instance of {@link InsertarAvion }
     * 
     */
    public InsertarAvion createInsertarAvion() {
        return new InsertarAvion();
    }

    /**
     * Create an instance of {@link InsertarRutas }
     * 
     */
    public InsertarRutas createInsertarRutas() {
        return new InsertarRutas();
    }

    /**
     * Create an instance of {@link InsertarRutasEscalas }
     * 
     */
    public InsertarRutasEscalas createInsertarRutasEscalas() {
        return new InsertarRutasEscalas();
    }

    /**
     * Create an instance of {@link InsertarVuelos }
     * 
     */
    public InsertarVuelos createInsertarVuelos() {
        return new InsertarVuelos();
    }

    /**
     * Create an instance of {@link InsertarVuelosResponse }
     * 
     */
    public InsertarVuelosResponse createInsertarVuelosResponse() {
        return new InsertarVuelosResponse();
    }

    /**
     * Create an instance of {@link InsertarEscalasResponse }
     * 
     */
    public InsertarEscalasResponse createInsertarEscalasResponse() {
        return new InsertarEscalasResponse();
    }

    /**
     * Create an instance of {@link InsertarRutasEscalasResponse }
     * 
     */
    public InsertarRutasEscalasResponse createInsertarRutasEscalasResponse() {
        return new InsertarRutasEscalasResponse();
    }

    /**
     * Create an instance of {@link InsertarBoletosResponse }
     * 
     */
    public InsertarBoletosResponse createInsertarBoletosResponse() {
        return new InsertarBoletosResponse();
    }

}
